﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerCharge : MonoBehaviour
{
    public Transform target;
    public bool charging = false;
    public Vector3 targetPosition;
    public int chargingSpeed;

	void Start ()
    {
		
	}
	
	void Update ()
    {
		if(charging)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, chargingSpeed * Time.deltaTime);
        }
        if(Vector3.Distance(transform.position, targetPosition)<2)
        {
            charging = false;
        }
	}
}
