﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class ButtonHoverGrowl : MonoBehaviour, ISelectHandler , IPointerEnterHandler
{
	public AudioClip growl1;
	public AudioClip growl2;
	public AudioClip growl3;
	public AudioClip growl4;
	AudioSource audioSource;

	private float count;

	// When highlighted with mouse.
	void Start()
	{
		audioSource = GetComponent<AudioSource>();
		count = 0;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (count == 0) {
			audioSource.clip = growl1;
			audioSource.Play();
			count = count + 1;
		}
		else if (count == 1) {
			audioSource.clip = growl2;
			audioSource.Play();
			count = count + 1;
		}
		else if (count == 2) {
			audioSource.clip = growl3;
			audioSource.Play();
			count = count + 1;
		}
		else if (count == 3) {
			audioSource.clip = growl4;
			audioSource.Play();
			count = 0;
		}
		else
		{
			
		}

	}

	public void OnSelect(BaseEventData eventData)
	{
		audioSource.clip = growl1;
		audioSource.Play();
	}
	// When selected.

}