﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionObject : MonoBehaviour
{
	public bool inventory;
	public bool openable;
	public bool locked, isKey;
	public bool isMedkit;
	public GameObject itemNeeded;

	public Animator anim;

    void Update()
    {
    }

	public void DoInteraction()
	{
		gameObject.SetActive (false);
	}

	public void Open()
	{
		anim.SetBool ("open", true);
	}

    public bool IsKey()
    {
        return isKey;
    }

    public bool IsMedkit()
    {
        return isMedkit;
    }
}
