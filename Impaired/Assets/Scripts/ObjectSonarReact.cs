﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSonarReact : MonoBehaviour
{
    public float duration = 3f;
    public float t = 0;
    public bool flag = true, sonarReact = false;
    SpriteRenderer rend;
    float transparencyAmount;
    public float transparency;
    Sonar playerSonar;
    Transform playerPosition;

    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        rend.color = new Color(1f, 1f, 1f, 0f);
        playerSonar = GameObject.FindGameObjectWithTag("Player").GetComponent<Sonar>();
        playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        transparency = 0f;
    }

    void FixedUpdate()
    {
        if (!flag)
        {
            transparency = transparency + 0.05f;
            rend.color = new Color(1f,1f,1f,transparency);
            t += Time.deltaTime / duration;
            if(t>0.99f||transparency>=transparencyAmount)
            {
                flag = true;
                t = 0;
            }
        }   

        if(Time.time > playerSonar.sonarDisplay)
        {
            sonarReact = true;
        }
        else
        {
            sonarReact = false;
        }

        if (Vector2.Distance(playerPosition.position, transform.position) < 15 && Input.GetKeyUp(KeyCode.Space) && sonarReact)
        {
            sonarReact = false;
            if (playerSonar.sonarCharge <= 200 && playerSonar.sonarCharge > 150)
            {
                transparencyAmount = 1f;
                flag = false;
            }
            else if (playerSonar.sonarCharge <= 150 && playerSonar.sonarCharge > 50)
            {
                transparencyAmount = 0.75f;
                flag = false;
            }
            else if (playerSonar.sonarCharge <= 50)
            {
                transparencyAmount = 0.5f;
                flag = false;
            }
        }
    }
}