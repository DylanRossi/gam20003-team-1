﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WallBump : MonoBehaviour
{
    public ParticleSystem wallBump;
    Vector3 position;

    void Start()
    {
        wallBump.Stop();
    }

    void Update()
    {
        wallBump.transform.position = position;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Math.Abs(collision.contacts[0].normal.x) > 0.5f)
        {
            wallBump.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            wallBump.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        wallBump.transform.position = new Vector3(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y, -2);
        position = collision.gameObject.transform.position;
        wallBump.Play();
    }
}
