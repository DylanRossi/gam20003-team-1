﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public GameObject[] inventory = new GameObject[5];

    void Start()
    {
    }

    public void AddItem(GameObject item)
    {
        bool itemAdded = false;

        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == null)
            {
                inventory[i] = item;
                Debug.Log(item.name + " was added");
                itemAdded = true;
                item.SendMessage("DoInteraction");
                break;
            }
        }

        if (!itemAdded)
        {
            Debug.Log("Inventory Full - Item not added");
        }
    }


    public bool FindItem(GameObject item)
    {

        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == item)
            {
                return true;
            }
        }
        return false;
    }

    public bool FindKey()
    {
        for(int i=0;i<inventory.Length;i++)
        {
            if(inventory[i]!=null)
            {
                if (inventory[i].GetComponent<InteractionObject>().IsKey())
                {
                    inventory[i] = null;
                    return true;
                }
            }
        }
        return false;
    }

}
