﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Bleeding : MonoBehaviour
{
    public bool bleeding = false, warning = true;
    public int timer = 120;
    public float time, medkitTimer;
    public string loseScreen;
	public AudioSource audioSource;
	public AudioClip moan;
	public AudioClip moan2;
    public AudioClip dying;
    public Text Countdown;
    public Text medkitWarning;

    void Start ()
    {
		audioSource = GetComponent<AudioSource>();
		//audioSource2 = GetComponent<AudioSource>();
        Countdown.enabled = false;
        medkitWarning.enabled = false;
	}
	
	void Update ()
    {
		if(bleeding)
        {
            if(warning)
            {
                medkitWarning.enabled = true;
                medkitTimer = Time.time + 10f;
                warning = false;
            }
            Countdown.enabled = true;
            Countdown.text = timer.ToString();
            if(Time.time > time)
            {
                timer--;
                time = Time.time + 1f;
            }
            if(timer == 0)
            {
                SceneManager.LoadScene(loseScreen);
                audioSource.PlayOneShot(dying);
            }
            if (Time.time > medkitTimer)
            {
                medkitWarning.enabled = false;
            }
        }
        
	}
}
