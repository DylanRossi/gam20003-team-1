﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningEffect : MonoBehaviour
{
    float timeToLightning = 300f, flashTime;
    bool flashActive = false, flash, watchFlash = true;
    int flashCount;

	public AudioClip lightning;
	AudioSource audioSource;

    void Start()
    {
		audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Time.time > timeToLightning && watchFlash)
        {
            int aNumber = Random.Range(1, 1000);
            if (aNumber == 1)
            {
                flashActive = true;
                flash = true;
                flashTime = Time.time + 0.2f;
                watchFlash = false;

				audioSource.clip = lightning;
				audioSource.Play ();
            }
        }

        if (flashActive)
        {
            if (Time.time > flashTime && flash)
            {
                flash = false;
                flashTime += 0.2f;
                Camera.main.backgroundColor = Color.white;
                flashCount++;
            }
            if (Time.time > flashTime && !flash)
            {
                flash = true;
                flashTime += 0.2f;
                Camera.main.backgroundColor = Color.black;
                flashCount++;
            }

        }

        if (flashCount >= 4)
        {
            flashActive = false;
            foreach (SpriteRenderer wall in GameObject.FindGameObjectWithTag("Wall").GetComponentsInChildren<SpriteRenderer>())
            {
                wall.color = new Color(1f, 1f, 1f, 0f);
            }
            foreach (SpriteRenderer floor in GameObject.FindGameObjectWithTag("Floor").GetComponentsInChildren<SpriteRenderer>())
            {
                floor.color = new Color(1f, 1f, 1f, 0f);
            }
            foreach (ObjectSonarReact wall in GameObject.FindGameObjectWithTag("Wall").GetComponentsInChildren<ObjectSonarReact>())
            {
                wall.transparency = 0f;
            }
            foreach (ObjectSonarReact floor in GameObject.FindGameObjectWithTag("Floor").GetComponentsInChildren<ObjectSonarReact>())
            {
                floor.transparency = 0f;
            }
            flashCount = 0;
            watchFlash = true;
            timeToLightning = Time.time + 300f;
        }
    }
}
