﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footprints : MonoBehaviour
{
    public float timeTillDeath;

	void Start ()
    {
        timeTillDeath = Time.time + 10f;
	}
	
	void Update ()
    {
		if(Time.time>timeTillDeath)
        {
            Destroy(gameObject);
        }
	}
}
