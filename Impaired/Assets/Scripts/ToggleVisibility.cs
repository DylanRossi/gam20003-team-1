﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleVisibility : MonoBehaviour 
{
	public GameObject GameObjectToHide;
	public float openTimeMin = 1.0f;
	public float openTimeMax = 5.0f;
	public float closeTimeMin = 1.0f;
	public float closeTimeMax = 5.0f;

	void Start()
	{
		
		StartCoroutine(ToggleVisibilityCo(GameObjectToHide));
	}

	IEnumerator ToggleVisibilityCo(GameObject someObj)
	{
		if (someObj == null) yield break;
		while (true) {
			someObj.SetActive (true);
			yield return new WaitForSeconds (Random.Range (openTimeMin, openTimeMax));
			someObj.SetActive (false);
			yield return new WaitForSeconds (Random.Range (closeTimeMin, closeTimeMax));
		}
	}
}