﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour {

	public Animator transitionAnim;
	public string sceneName;
	public float delay = 5f;

	void Update()
	{
		StartCoroutine (LoadLevel (delay));
	}

	IEnumerator LoadLevel(float delay){
		transitionAnim.SetTrigger ("end");
		yield return new WaitForSeconds (delay);
		SceneManager.LoadScene (sceneName);
	}
}
