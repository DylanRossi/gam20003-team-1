﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreenToStart : MonoBehaviour {

	public float delayTime = 6f;
	public string backtomenu;

	void Start()
	{
		Invoke ("DelayedAction", delayTime);
	}

	public void DelayedAction()
	{
		SceneManager.LoadScene(backtomenu);
	}


}