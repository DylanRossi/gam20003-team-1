﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class ButtonHover : MonoBehaviour, ISelectHandler , IPointerEnterHandler
{
	public AudioClip buttonhover;
	public AudioClip buttonPress;
	AudioSource audioSource;

	// When highlighted with mouse.
	void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		audioSource.clip = buttonhover;
		audioSource.Play();

	}
	// When selected.
	public void OnSelect(BaseEventData eventData)
	{
		audioSource.clip = buttonPress;
		audioSource.Play();
	}
}