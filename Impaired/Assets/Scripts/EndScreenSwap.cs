﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenSwap : MonoBehaviour {
    float timeToLightning = 1f, flashTime;
    bool flashActive = false, flash, watchFlash = true;
    int flashCount = 0;
    Image endText, screenNoBlood, screenBlood;

	void Start ()
    {
        screenNoBlood = GameObject.FindGameObjectWithTag("screenNoBlood").GetComponentInChildren<Image>();
        screenBlood = GameObject.FindGameObjectWithTag("screenBlood").GetComponentInChildren<Image>();
        endText = GameObject.FindGameObjectWithTag("endText").GetComponentInChildren<Image>();
        endText.enabled = false;
        screenBlood.enabled = false;
    }
	
	void Update () {
		if(Time.time>timeToLightning&&watchFlash)
        {
            flashActive = true;
            flash = true;
            flashTime = Time.time + 0.2f;
            watchFlash = false;
        }

        if(flashActive)
        {
            if(Time.time>flashTime&&flash)
            {
                flash = false;
                flashTime += 0.2f;
                screenNoBlood.enabled = false;
                flashCount++;
            }
            if(Time.time>flashTime&&!flash)
            {
                flash = true;
                flashTime += 0.2f;
                screenNoBlood.enabled = true;
                flashCount++;
            }
        }

        if(flashCount>=4)
        {
            flashActive = false;
            screenNoBlood.enabled = false;
            screenBlood.enabled = true;
            endText.enabled = true;
        }
	}
}
