﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding.Util;
using System;

public class KillerMovement : MonoBehaviour
{

    bool sonarPinged = false;
    float sonarPingedTime;
    public float stoppingDistance;
    Animator killerAnimator;
    public AnimationClip left;
    public AnimationClip right;
    public AnimationClip up;
    public AnimationClip down;
    public AnimationClip NE;
    public AnimationClip NW;
    public AnimationClip SE;
    public AnimationClip SW;
    double killerOldPositionY, killerOldPositionX, killerNewPositionY, killerNewPositionX;
    public Transform trail;
    public float trailTime = 0.5f;
    public float totalTime;

    void Start()
    {
        killerAnimator = GetComponent<Animator>();
        killerOldPositionX = Math.Round(transform.position.x, 1);
        killerOldPositionY = Math.Round(transform.position.y, 1);
    }

    void Update()
    {
        totalTime += Time.deltaTime;
        killerNewPositionX = Math.Round(transform.position.x, 1);
        killerNewPositionY = Math.Round(transform.position.y, 1);
        var target = GameObject.FindGameObjectWithTag("Player");
        if (killerOldPositionY > killerNewPositionY && killerOldPositionX > killerNewPositionX)
        {
            killerAnimator.Play("KillerAnimSW");
        }
        else if (killerOldPositionY < killerNewPositionY && killerOldPositionX > killerNewPositionX)
        {
            killerAnimator.Play("KillerAnimNW");
        }
        else if (killerOldPositionY > killerNewPositionY && killerOldPositionX < killerNewPositionX)
        {
            killerAnimator.Play("KillerAnimSE");
        }
        else if (killerOldPositionY < killerNewPositionY && killerOldPositionX < killerNewPositionX)
        {
            killerAnimator.Play("KillerAnimNE");
        }
        else if (killerOldPositionX > killerNewPositionX)
        {
            killerAnimator.Play("KillerAnimLeft");
        }
        else if (killerOldPositionX < killerNewPositionX)
        {
            killerAnimator.Play("KillerAnimRight");
        }
        else if (killerOldPositionY > killerNewPositionY)
        {
            killerAnimator.Play("KillerAnimDown");
        }
        else if (killerOldPositionY < killerNewPositionY)
        {
            killerAnimator.Play("KillerAnimUp");
        }

        if (totalTime > trailTime)
        {
            Instantiate(trail, GetComponent<Transform>().position, trail.rotation);
            totalTime = 0;
        }

        if (target.GetComponent<Sonar>().sonarOn)
        {
            sonarPinged = true;
            sonarPingedTime = Time.time + 6.0f;
        }

        if (Time.time > sonarPingedTime)
        {
            sonarPinged = false;
        }

        if (sonarPinged && !GetComponent<KillerCharge>().charging)
        {
            GetComponent<Wander>().enabled = false;
            GetComponent<Pathfinding.AILerp>().enabled = true;
        }
        else if (Vector2.Distance(transform.position, target.transform.position) < stoppingDistance && !GetComponent<KillerCharge>().charging)
        {
            GetComponent<Wander>().enabled = false;
            GetComponent<Pathfinding.AILerp>().enabled = false;
            GetComponent<KillerCharge>().targetPosition = target.transform.position;
            GetComponent<KillerCharge>().charging = true;
        }
        else if(!GetComponent<KillerCharge>().charging)
        {
            GetComponent<Wander>().enabled = true;
            GetComponent<Pathfinding.AILerp>().enabled = false;
        }

        killerOldPositionX = killerNewPositionX;
        killerOldPositionY = killerNewPositionY;
    }
}