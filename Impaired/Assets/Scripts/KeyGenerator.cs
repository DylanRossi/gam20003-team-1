﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class KeyGenerator : MonoBehaviour {
    List<Transform> children;

	void Start ()
    {
        System.Random random = new System.Random();
        string[] lines = System.IO.File.ReadAllLines("Assets/KeyCoords.txt");
        List<string[]> coordList = new List<string[]>();
        children = new List<Transform>();
        foreach (string line in lines)
        {
            coordList.Add(line.Split());
        }

		foreach(Transform child in transform)
        {
            children.Add(child);
        }
        foreach(Transform key in children)
        {
            int randomNumber = random.Next(coordList.Count);
            key.position = new Vector3(float.Parse(coordList[randomNumber][0]), float.Parse(coordList[randomNumber][1]), -1);
            coordList.Remove(coordList[randomNumber]);
        }
	}
	
	void Update ()
    {
    }
}
