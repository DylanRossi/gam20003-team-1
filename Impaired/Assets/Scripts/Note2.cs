﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Note2 : MonoBehaviour {

	public Image noteImage;
	public GameObject HideNoteButton;

	public AudioClip pickupSound;
	public AudioClip putAwaySound;

	public GameObject playerObject;

	// Use this for initialization

	void Start () {
		noteImage.enabled = false;
		HideNoteButton.SetActive (false);

	}
	
	public void ShowNoteImage()
	{
		noteImage.enabled = true;
		GetComponent<AudioSource> ().PlayOneShot (pickupSound);

		HideNoteButton.SetActive (true);

		// Disable the Player - Controller
		playerObject.GetComponent<Mobility>().enabled = false;


	//	Cursor.lockState = CursorLockMode.None;
	//	Cursor.visible = true;
	}

	public void HideNoteImage()
	{
		noteImage.enabled = false;
		GetComponent<AudioSource> ().PlayOneShot (putAwaySound);

		HideNoteButton.SetActive (false);
		Time.timeScale = 1f;

	//	Cursor.lockState = CursorLockMode.Locked;
	//	Cursor.visible = false;

		playerObject.GetComponent<Mobility>().enabled = true;
	}

}
