﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwap : MonoBehaviour
{
    public string sceneName;

    public float waitTime;
    public Animator musicAnim;

    void Start()
    {
        musicAnim = GetComponent<Animator>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(ChangeScene());
        }
    }

    IEnumerator ChangeScene()
    {
        //musicAnim.SetTrigger("fadeout");
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene(sceneName);
    }

    public void OnMouseClick()
    {
        musicAnim.SetTrigger("fadeout");    
        SceneManager.LoadScene("Julian Map");
    }
}