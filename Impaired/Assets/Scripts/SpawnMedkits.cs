﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpawnMedkits : MonoBehaviour
{
    List<Transform> children;
    List<Vector2> coords;

	void Start ()
    {
        coords = new List<Vector2>();
        children = new List<Transform>();
        System.Random random = new System.Random();

        coords.Add(new Vector2(-12.3f, -63.8f));
        coords.Add(new Vector2(-90.8f, 30.8f));
        coords.Add(new Vector2(-104.4f, 69.7f));
        coords.Add(new Vector2(-20.6f, 75.5f));
        coords.Add(new Vector2(52f, 76f));
        coords.Add(new Vector2(99.8f, 40.9f));

        foreach (Transform child in transform)
        {
            children.Add(child);
        }

        foreach(Transform medkit in children)
        {
            int randNo = random.Next(coords.Count);
            medkit.position = coords[randNo];
            coords.Remove(coords[randNo]);
        }
	}
	
	void Update ()
    {
		
	}
}
