﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitRandomiser : MonoBehaviour {
    public List<Vector2> exitCoords;
    Scene aScene;

	// Use this for initialization
	void Start () {
        aScene = SceneManager.GetActiveScene();
        exitCoords = new List<Vector2>();
        if (aScene.name == "Julian Map")
        {
            exitCoords.Add(new Vector2(-232.49f, -85.68f));
            exitCoords.Add(new Vector2(-157.1f, -81.6f));
            exitCoords.Add(new Vector2(-23.2f, -116.8f));
            exitCoords.Add(new Vector2(-142.61f, -218.66f));
        }
        if (aScene.name=="Julian Map 1" || aScene.name == "Julian Map 2" || aScene.name == "Julian Map 3")
        {
            exitCoords.Add(new Vector2(-233.167f, 119.2979f));
            exitCoords.Add(new Vector2(27.09f, -94.24f));
            exitCoords.Add(new Vector2(146.9f, 16.75f));
            exitCoords.Add(new Vector2(-172.27f, -49.77f));
        }
        int randNo = Random.Range(0, 3);
        transform.position = exitCoords[randNo];
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
