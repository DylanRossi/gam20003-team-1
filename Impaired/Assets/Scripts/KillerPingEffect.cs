﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerPingEffect : MonoBehaviour
{
    ParticleSystem ping;
    float sonarDisplay = 0.0f;
    float sonarDisplayOff = 0.0f;
    bool playKillerPing = false;
    float xValue;
    float yValue;

    void Start()
    {
        ping = GetComponent<ParticleSystem>();
        ping.Stop();
    }

    void Update()
    {
        var killer = GameObject.FindGameObjectWithTag("Enemy");
        var player = GameObject.FindGameObjectWithTag("Player");
        bool sonarOn = player.GetComponentInChildren<Sonar>().sonarOn;

        if (sonarOn)
        {
            if (Vector3.Distance(killer.transform.position, player.transform.position) > 30)
            {
                if (killer.transform.position.x <= player.transform.position.x - 30)
                {
                    xValue = 0;
                }
                else if (killer.transform.position.x >= player.transform.position.x + 30)
                {
                    xValue = 1;
                }
                else
                {
                    xValue = 0.5f;
                }

                if (killer.transform.position.y <= player.transform.position.y - 30)
                {
                    yValue = 0;
                }
                else if (killer.transform.position.y >= player.transform.position.y + 30)
                {
                    yValue = 1;
                }
                else
                {
                    yValue = 0.5f;
                }
                sonarDisplay = Time.time + 3.0f;
                playKillerPing = true;
            }
        }

        ping.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(xValue, yValue, 5));

        if (Time.time > sonarDisplay && playKillerPing)
        {
            ping.Play();
            sonarDisplayOff = Time.time + 3.0f;
            playKillerPing = false;
        }
        if (Time.time > sonarDisplayOff)
        {
            ping.Stop();
        }
    }
}
