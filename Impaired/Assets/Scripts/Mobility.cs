﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mobility : MonoBehaviour
{
	private Rigidbody2D rb;

	public int speed;
	public GameObject rightFootprint, leftFootprint;
	Transform footprints;
	public float totalTime;
	public float footprintTime = 0.5f;
	bool switchFootprints;

	public float dashRate;
	private float dash;
	private float dashCoolDown;
	public float dashCoolDownRate;
	private float dashRegen, dashTimerCount;
	private bool activated = false;
	public int dashMultiplier, dashTimer;

	AudioSource audioSource;
	public AudioClip footstep;

    public Sprite leftNormalFootprint;
    public Sprite rightNormalFootprint;
    public Sprite leftBloodyFootprint;
    public Sprite rightBloodyFootprint;

    public GameObject killer;

    public Text dashCountdown;

	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource>();
        // This code works however when you change scene the cursor is still locked
        //	Cursor.lockState = CursorLockMode.Locked;
        //	Cursor.visible = false;
        dashTimer = 0;
        dashCountdown.enabled = false;
	}

	void Update()
	{
        dashCountdown.text = "Dash: "+dashTimer.ToString();
        if(GetComponent<Bleeding>().bleeding)
        {
            leftFootprint.GetComponent<SpriteRenderer>().sprite = leftBloodyFootprint;
            rightFootprint.GetComponent<SpriteRenderer>().sprite = rightBloodyFootprint;
        }
        else
        {
            leftFootprint.GetComponent<SpriteRenderer>().sprite = leftNormalFootprint;
            rightFootprint.GetComponent<SpriteRenderer>().sprite = rightNormalFootprint;
        }
		if(switchFootprints)
		{
			footprints = rightFootprint.transform;
		}
		else
		{
			footprints = leftFootprint.transform;
		}
		totalTime += Time.deltaTime;
		if (Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)))
		{
			if (totalTime > footprintTime)
			{
				footprints.rotation = Quaternion.Euler(0, 0, 270);
				Instantiate(footprints, GetComponent<Transform>().position, footprints.rotation);
				totalTime = 0;
				switchFootprints = !switchFootprints;
				audioSource.clip = footstep;
				audioSource.pitch = .8f;
				audioSource.volume = 0.2f;
				audioSource.Play();
			}
			rb.velocity = new Vector2(speed, rb.velocity.y);
		}
		else if (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)))
		{
			if (totalTime > footprintTime)
			{
				footprints.rotation = Quaternion.Euler(0, 0, 90);
				Instantiate(footprints, GetComponent<Transform>().position, footprints.rotation);
				totalTime = 0;
				switchFootprints = !switchFootprints;
				audioSource.clip = footstep;
				audioSource.pitch = .8f;
				audioSource.volume = 0.2f;
				audioSource.Play();

			}
			rb.velocity = new Vector2(speed * -1, rb.velocity.y);
		}
		else
		{
			rb.velocity = new Vector2(0, rb.velocity.y);
		}

		if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)))
		{
			if (totalTime > footprintTime)
			{
				footprints.rotation = Quaternion.Euler(0, 0, 0);
				Instantiate(footprints, GetComponent<Transform>().position, footprints.rotation);
				totalTime = 0;
				switchFootprints = !switchFootprints;
				audioSource.clip = footstep;
				audioSource.pitch = .8f;
				audioSource.volume = 0.2f;
				audioSource.Play();
			}
			rb.velocity = new Vector2(rb.velocity.x, speed);
		}
		else if (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)))
		{
			if (totalTime > footprintTime)
			{
				footprints.rotation = Quaternion.Euler(0, 0, 180);
				Instantiate(footprints, GetComponent<Transform>().position, footprints.rotation);
				totalTime = 0;
				switchFootprints = !switchFootprints;
				audioSource.clip = footstep;
				audioSource.pitch = .8f;
				audioSource.volume = 0.2f;
				audioSource.Play();
			}
			rb.velocity = new Vector2(rb.velocity.x, speed * -1);
		}
		else
		{
			rb.velocity = new Vector2(rb.velocity.x, 0);
		}
		if ((Input.GetKey (KeyCode.LeftShift)) && (Time.time > dash) && !activated)
        {
            if(killer.GetComponent<KillerCharge>().charging)
            {
                GetComponent<Bleeding>().bleeding = true;
                GetComponent<Bleeding>().time = Time.time + 1f;
            }
            dashCountdown.enabled = true;
			speed = speed * dashMultiplier;
			dashCoolDown = Time.time + dashCoolDownRate;
			activated = true;
			dash = Time.time + dashRate;
            dashTimer = 60;
            dashTimerCount = Time.time + 1f;
		}
		if (Time.time > dashCoolDown && activated) {
			speed = speed / dashMultiplier;
            activated = false;
		}
        if(Time.time > dashTimerCount)
        {
            dashTimer--;
            dashTimerCount = Time.time + 1f;
        }
        if(dashTimer<0)
        {
            dashTimer = 0;
            dashCountdown.enabled = false;
        }
	}
}
