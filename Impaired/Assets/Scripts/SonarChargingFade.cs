﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SonarChargingFade : MonoBehaviour {
	public GameObject light;
	public Color colorController;
	private float chargeAmount;
	void Start () {
		chargeAmount = 0.0f;
		colorController = light.GetComponent<SpriteRenderer> ().color;
		colorController.a = 0f;
		light.GetComponent<SpriteRenderer> ().color = colorController;
	}

	public void RunFade (int maxRate) {
		chargeAmount += 1f;
		colorController.a += chargeAmount/(maxRate*10);
		light.GetComponent<SpriteRenderer> ().color = colorController;
		//color.a = 0f;
		//light.GetComponent<SpriteRenderer> ().color = color;
	}
	public void Reset () {
		chargeAmount = 0;
		colorController.a = 0f;
		light.GetComponent<SpriteRenderer> ().color = colorController;
	}
}
