﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterRandomiser : MonoBehaviour
{
    List<Vector2> coords;

	void Start ()
    {
        coords = new List<Vector2>();
        coords.Add(new Vector2(-64.8f, -35.2f));
        coords.Add(new Vector2(16.7f, -71.9f));
        coords.Add(new Vector2(47.9f, 5.5f));
        System.Random random = new System.Random();
        int randNo = random.Next(coords.Count);
        transform.position = coords[randNo];
	}

	void Update ()
    {
		
	}
}
