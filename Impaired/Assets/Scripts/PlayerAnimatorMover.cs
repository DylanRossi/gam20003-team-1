﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorMover : MonoBehaviour
{

    Animator anim;
    public AnimationClip left;
    public AnimationClip right;
    public AnimationClip up;
    public AnimationClip down;
    public AnimationClip NW;
    public AnimationClip NE;
    public AnimationClip SE;
    public AnimationClip SW;
    GameObject player;
    SpriteRenderer rend;
    Sonar playerSonar;

    void Start ()
    {
        anim = GetComponent<Animator>();
        rend = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerSonar = player.GetComponent<Sonar>();
	}
	
	void Update ()
    {
        transform.position = player.transform.position;
        if(playerSonar.sonarCharge < playerSonar.sonarChargeNeeded)
        {
            rend.color = new Color(1f, 0.6f, 0.2f, 1f);
        }
        else
        {
            rend.color = Color.white;
        }

        if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)))
        {
            anim.Play("PlayerAnimNW");
        }
        else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)))
        {
            anim.Play("PlayerAnimNE");
        }
        else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)))
        {
            anim.Play("PlayerAnimSE");
        }
        else if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)))
        {
            anim.Play("PlayerAnimSW");
        }
        else if (Input.GetKey(KeyCode.D)||Input.GetKey(KeyCode.RightArrow))
        {
            anim.Play("PlayerAnimRight");
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            anim.Play("PlayerAnimLeft");
        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            anim.Play("PlayerAnimUp");
        }
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            anim.Play("PlayerAnimDown");
        }
       
    }
}
