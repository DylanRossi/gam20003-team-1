﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonar : MonoBehaviour
{
    public ParticleSystem mySonar;
    public int sonarCharge, sonarChargeNeeded;
    public float nextSonar, sonarReadyTimer;
    public float sonarRate;
    public float sonarOff;
    public float sonarOffRate;
    public bool sonarOn = false, sonarReadyCheck = false;
    public float sonarDisplayRate = 3.0f;
    public float sonarDisplay = 0.0f;
    //SonarAnimationObject
    public SonarChargingFade sonarAnim;

	public GameObject playerEyes;
	public GameObject playerEyes1;

    //AudioSource audioSourceSonar;
    //public AudioClip pingnoise;
    public AudioSource[] sounds;
    public AudioSource noise1;
    public AudioClip sonar3;
    public AudioClip sonarReady;

    CameraShake camShake;

    void Start()
    {
        mySonar.Stop();
        sounds = GetComponents<AudioSource>();
        noise1 = sounds[0];
        camShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
        camShake.shaketrue = false;
        camShake.shakeDuration = 1;
        sonarChargeNeeded = 200;
        sonarCharge = sonarChargeNeeded;
		playerEyes.SetActive (false);
		playerEyes1.SetActive (true);
    }

    void Update()
    {
        var sonarMain = mySonar.main;
        if (Input.GetKeyUp(KeyCode.Space) && Time.time > nextSonar)
        {
            if (sonarCharge <=200 && sonarCharge > 150)
            {
                sonarMain.startSize = 10;
            }
            else if (sonarCharge <= 150 && sonarCharge > 50)
            {
                sonarMain.startSize = 5;
            }
            else
            {
                sonarMain.startSize = 2;
            }

            sonarAnim.Reset();

            sonarOn = true;
            sonarOff = Time.time + sonarOffRate;
            sonarDisplay = Time.time + sonarDisplayRate;
            mySonar.Play();
            camShake.shaketrue = true;
            noise1.PlayOneShot(sonar3);
            nextSonar = Time.time + 3f;
            sonarReadyCheck = true;
            sonarReadyTimer = Time.time + 3f;
            //  audioSourceSonar.clip = pingnoise;
            //audioSourceSonar.pitch = 0.3f;
            //audioSourceSonar.volume = 0.5f;
            //audioSourceSonar.Play ();
            sonarCharge = 0;

			playerEyes.SetActive (true);
			playerEyes1.SetActive (false);

        }
        if (Time.time > sonarOff)
        {
            sonarOn = false;

        }
        if(Time.time > sonarReadyTimer && sonarReadyCheck)
        {
           // noise1.clip = sonarReady;
           // noise1.PlayOneShot(sonarReady);
            sonarReadyCheck = false;
			playerEyes.SetActive (false);
			playerEyes1.SetActive (true);
        }
        if (Time.time > sonarDisplay)
        {
            camShake.shaketrue = false;
            camShake.shakeDuration = 1;
            mySonar.Stop();
        }
        if (sonarCharge < sonarChargeNeeded && Time.time > nextSonar)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                sonarCharge++;
                sonarAnim.RunFade(sonarChargeNeeded);
            }
        }
        if (sonarCharge > sonarChargeNeeded)
        {
            sonarCharge = sonarChargeNeeded;
        } 
    }
}