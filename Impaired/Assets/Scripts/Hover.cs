﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

	public Texture2D mouseDefault;
//	public Texture2D mousePress;
	public CursorMode curMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;

	// Use this for initialization
	void Start () {
		Cursor.SetCursor (mouseDefault, hotSpot, curMode);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

//	void OnMouseEnter()
//	{
//		if (gameObject.tag == "Note") {
	//		Cursor.SetCursor (mouseDefault, hotSpot, curMode);
	//	}
//	}

//	void OnMouseExit()
	//{
//		Cursor.SetCursor (mousePress, hotSpot, curMode);
	//}
}
