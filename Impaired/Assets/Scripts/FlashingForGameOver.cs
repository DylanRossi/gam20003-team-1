﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingForGameOver : MonoBehaviour
{
	float timeToLightning = 300.0f, flashTime;
	bool flashActive = false, flash;
	int flashCount;

	void Start()
	{
	}

	void Update()
	{
				flashActive = true;
				flash = true;
				flashTime = Time.time + 0.2f;

		if (Input.GetKeyDown(KeyCode.P))
		{
			flashActive = true;
			flash = true;
			flashTime = Time.time + 0.2f;

		}

		if (flashActive)
		{
			if (Time.time > flashTime && flash)
			{
				flash = false;
				flashTime += 0.2f;
				Camera.main.backgroundColor = Color.white;
				flashCount++;
			}
			if (Time.time > flashTime && !flash)
			{
				flash = true;
				flashTime += 0.2f;
				Camera.main.backgroundColor = Color.black;
				flashCount++;
			}

		}

		/*if (flashCount >= 4)
		{
			flashActive = false;
			foreach (Renderer wall in GetComponentsInChildren<Renderer>())
			{
				wall.material.color = Color.black;
				var wallProperties = wall.GetComponent<ObjectSonarReact>();
				wallProperties.flag = true;
				wallProperties.colorIni = Color.black;
				wallProperties.colorFin = Color.white;
				wallProperties.t = 0;
			}
			flashCount = 0;
		}*/
	}
}
