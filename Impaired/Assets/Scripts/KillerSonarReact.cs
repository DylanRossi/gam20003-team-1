﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerSonarReact : MonoBehaviour
{
    SpriteRenderer s_renderer;
	ParticleSystem playerDeath;

    public int firstRadius;
    public int secondRadius;
    public int thirdRadius;
    public Shake shake;

    public AudioClip firstWarn;
	public AudioClip secondWarn;
	public AudioClip thirdWarn;

    AudioSource audioSource;

    void Start()
    {
        s_renderer = GetComponent<SpriteRenderer>();
		playerDeath = GetComponentInChildren<ParticleSystem>();
		playerDeath.Stop ();
        audioSource = GetComponent<AudioSource>();
        shake = GameObject.FindGameObjectWithTag("ScreenShake").GetComponent<Shake>();
    }

    void Update()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        if ((Vector2.Distance(transform.position, player.transform.position) < firstRadius) && (player.GetComponentInChildren<ParticleSystem>().emission.enabled))
        {
            s_renderer.color = Color.red;
		//	if (!audioSource.isPlaying) {
		//		audioSource.clip = thirdWarn;
		//		audioSource.Play ();
		//	}
				shake.CamShake();


        }
        else if ((Vector2.Distance(transform.position, player.transform.position) < secondRadius) && (player.GetComponentInChildren<ParticleSystem>().emission.enabled))
        {
            s_renderer.color = Color.green;
			//if (!audioSource.isPlaying) {
		//		audioSource.clip = secondWarn;
		//		audioSource.Play ();
		//	}
				shake.CamShake();
        }
        else if ((Vector2.Distance(transform.position, player.transform.position) < thirdRadius) && (player.GetComponentInChildren<ParticleSystem>().emission.enabled))
        {
            s_renderer.color = Color.cyan;
		//	audioSource.PlayOneShot (firstWarn);
                shake.CamShake();
				
        }
        else
        {
            s_renderer.color = Color.blue;
        }
    }

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag ("Player"))
        {
			playerDeath.Play ();
		}
	}

}
